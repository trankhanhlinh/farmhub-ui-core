import styled from 'styled-components';
import { Input as AntInput } from 'antd';

import { theme } from 'src/theme';

export const Search = styled(AntInput.Search)<{
  width?: string;
  height?: string;
  background?: string;
  fontSize?: string;
  fontWeight?: string;
  styles?: any;
}>`
  width: ${p => p.width || 'auto'};
  height: ${p => p.height || '48px'};

  .ant-input-wrapper.ant-input-group {
    height: ${p => p.height || '48px'};

    .ant-input-affix-wrapper {
      background: ${p => p.background || '#fff'};
      height: ${p => p.height || '48px'};
      font-size: ${p => theme.font.size[p.fontSize || 'xs']};

      .ant-input {
        background: ${p => p.background || '#fff'};
        font-size: ${p => theme.font.size[p.fontSize || 'xs']};
        font-weight: ${p => theme.font.weight[p.fontWeight || 'regular']};
        &::placeholder {
          font-size: ${p => theme.font.size[p.fontSize || 'xs']};
        }
      }
    }

    .ant-input-group-addon {
      button {
        height: ${p => p.height || '48px'};
        span {
          font-size: ${p => theme.font.size[p.fontSize || 'xs']};
          font-weight: ${p => p.theme.font.weight.bold} !important;
        }
      }
    }
  }
  ${p => p.styles}
`;
