import styled from 'styled-components';

const Column = styled.div<{
  width?: string;
  justifyContent?: string;
  alignItems?: string;
  styles?: any;
}>`
  width: ${p => p.width || '100%'};
  display: flex;
  flex-direction: column;
  justify-content: ${p => p.justifyContent || 'center'};
  align-items: ${p => p.alignItems || 'flex-start'};
  ${p => p.styles};
`;

export default Column;
