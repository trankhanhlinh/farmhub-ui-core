import styled, { css } from 'styled-components';
import { Tabs as AntTabs } from 'antd';

import { theme } from 'src/theme';
import Row from '../Row';
import Space from '../Space';

const LeftTabsStyles = css<{
  width?: string;
  fontSize?: string;
  fontWeight?: string;
  tabPadding?: string;
}>`
  .ant-tabs-nav {
    width: ${p => p.width || '160px'};
  }
  .ant-tabs-tab {
    margin: 0px !important;
    padding: ${p => p.tabPadding || '18px 24px'} !important;
    box-sizing: content-box;

    .ant-tabs-tab-btn {
      font-size: ${p => theme.font.size[p.fontSize || 'xs']};
    }

    &.ant-tabs-tab-active {
      background-color: rgba(5, 182, 114, 0.05);

      .ant-tabs-tab-btn {
        font-weight: ${p => theme.font.weight[p.fontWeight || 'medium']};
      }
    }
  }
  .ant-tabs-tabpane {
    padding-left: 0px !important;
  }
`;

const TopTabsStyles = css<{
  fontSize?: string;
  fontWeight?: string;
  tabPadding?: string;
  tabPaneActivePadding?: string;
}>`
  .ant-tabs-tab {
    margin: 0;
    padding: ${p => p.tabPadding || '18px 36px'} !important;

    .ant-tabs-tab-btn {
      font-size: ${p => theme.font.size[p.fontSize || 'xs']};
      font-weight: ${p => theme.font.weight[p.fontWeight || 'bold']};
    }
  }

  .ant-tabs-tabpane {
    &.ant-tabs-tabpane-active {
      padding: ${p => p.tabPaneActivePadding || '0px 6px 6px'};
    }
    ${Row} > ${Space}:last-child {
      display: none;
    }
  }
`;

export const Tabs = styled(AntTabs)<{
  tabPosition: string;
  styles?: any;
  marginBottom?: string;
}>`
  .ant-tabs-nav {
    ${p => p.marginBottom && `margin-bottom: ${p.marginBottom};`};
  }

  ${p => p.tabPosition === 'left' && LeftTabsStyles}
  ${p => p.tabPosition === 'top' && TopTabsStyles}
  ${p => p.styles}
`;
