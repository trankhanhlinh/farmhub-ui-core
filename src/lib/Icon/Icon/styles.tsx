import styled, { css } from 'styled-components';

const SvgStyles = css<{
  svgProps?: {
    isActive?: boolean;
    activeColor?: string;
    hoverColor?: string;
  };
}>`
  & svg {
    ${p => (p.svgProps?.isActive ? `fill: ${p.svgProps?.activeColor || 'rgba(5, 182, 114, 1)'}` : '')};
    path {
      ${p => (p.svgProps?.isActive ? `fill: ${p.svgProps?.activeColor || 'rgba(5, 182, 114, 1)'}` : '')};
    }
  }
  &:hover {
    svg {
      ${p => (p.svgProps?.hoverColor ? `fill: ${p.svgProps?.hoverColor || 'rgba(5, 182, 114, 1)'}` : '')};
      path {
        ${p => (p.svgProps?.hoverColor ? `fill: ${p.svgProps?.hoverColor || 'rgba(5, 182, 114, 1)'}` : '')};
      }
    }
  }
`;

export const Wrapper = styled.div<{
  svgProps?: {
    isActive?: boolean;
    activeColor?: string;
    hoverColor?: string;
  };
  width?: string;
  height?: string;
  onClick?: (...args: any[]) => any;
  styles?: any;
}>`
  display: flex;
  justify-content: center;
  align-items: center;
  ${SvgStyles}
  ${p => p.width && `width: ${p.width};`}
  ${p => p.height && `height: ${p.height};`}
  ${p =>
    p.onClick &&
    css`
      cursor: pointer;
    `}
  ${p => p.styles}
`;
