import * as React from 'react';

import * as S from './styles';

const Pagination = ({ ...props }: PaginationProps): JSX.Element => <S.Pagination {...props} />;

export default Pagination;

export interface PaginationProps {
  current: number;
  defaultCurrent?: number;
  defaultPageSize?: number;
  disabled?: boolean;
  hideOnSinglePage?: boolean;
  pageSize: number;
  pageSizeOptions?: string[];
  responsive?: boolean;
  total: number;
  onChange?: (page: number, pageSize?: number) => void;
  styles?: any;
}
