import * as React from 'react';

import * as S from './styles';

export enum DrawerPlacement {
  Top = 'top',
  Right = 'right',
  Bottom = 'bottom',
  Left = 'left',
}

const Drawer = ({ children, ...props }: DrawerProps): JSX.Element => <S.Drawer {...props}>{children}</S.Drawer>;

export default Drawer;

export interface DrawerProps {
  closable?: boolean;
  visible?: boolean;
  onClose?: (e?: any) => void;
  placement?: DrawerPlacement;
  width?: string;
  height?: string;
  styles?: any;
  children: React.ReactNode;
}
