import * as React from 'react';

import { theme } from 'src/theme';

import * as S from './styles';

const Icon = ({ children, svgProps, onClick, ...props }: IconProps): JSX.Element => {
  let icon: React.ReactNode = children;
  if (svgProps) {
    const { width = '18', height = '18', color = theme.colors.text.primary, style = {} } = svgProps;
    const { paths, viewBox = '0 0 24 24' } = svgProps.icon;
    icon = (
      <svg
        width={width}
        height={height}
        viewBox={viewBox}
        fill={color}
        style={style}
        xmlns="http://www.w3.org/2000/svg"
      >
        {paths.map(path => (
          <path fill={color} d={path} />
        ))}
      </svg>
    );
  }
  return (
    <S.Wrapper onClick={onClick} svgProps={svgProps} {...props}>
      {icon}
    </S.Wrapper>
  );
};

export default Icon;

export interface SvgProps {
  icon: {
    paths: string[];
    viewBox?: string;
  };
  isActive?: boolean;
  activeColor?: string;
  hoverColor?: string;
  color?: string;
  height?: string;
  width?: string;
  style?: any;
}

export interface IconProps {
  className?: string;
  svgProps?: SvgProps;
  height?: string;
  width?: string;
  styles?: any;
  children?: React.ReactNode;
  onClick?: (...args: any[]) => any;
}
