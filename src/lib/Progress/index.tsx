import * as React from 'react';

import Row from '../Row';
import Text from '../Text';

import * as S from './styles';

const Progress = ({ height = 22, color, percent, remainingDays }: ProgressProps): JSX.Element => (
  <S.Wrapper height={height}>
    <S.Percent width={percent} color={color} />
    <S.Info>
      <Row height="inherit" justifyContent="space-between">
        <Text lineHeight="unset" color="white" size="xxs" weight="medium">
          {percent}%
        </Text>
        {remainingDays && (
          <Text lineHeight="unset" color="white" size="xxs" weight="medium">
            Còn {remainingDays} ngày
          </Text>
        )}
      </Row>
    </S.Info>
  </S.Wrapper>
);

export default Progress;

export interface ProgressProps {
  height?: number;
  color?: string;
  percent: number;
  remainingDays?: number;
}
