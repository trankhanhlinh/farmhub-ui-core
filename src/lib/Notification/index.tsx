import { notification } from 'antd';

const Notification = (
  type: string,
  message: string,
  description: string | undefined = undefined,
) => {
  notification[type]({
    message,
    description,
  });
};

export default Notification;
