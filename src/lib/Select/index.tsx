import * as React from 'react';
import { Select as AntSelect } from 'antd';

import { Icon, IconProps } from '../Icon';

import * as S from './styles';

export const SelectOption = ({ children, ...props }: OptionProps): JSX.Element => (
  <AntSelect.Option {...props}>{children}</AntSelect.Option>
);

const Select = ({ children, icon, containerId, ...props }: SelectProps): JSX.Element => (
  <S.Select
    {...props}
    {...(containerId && { getPopupContainer: () => document.getElementById(containerId) as HTMLElement })}
    {...(icon && { suffixIcon: <Icon {...icon}>{icon.children}</Icon> })}
  >
    {children}
  </S.Select>
);

export default Select;

export interface SelectProps {
  placeholder?: string;
  icon?: IconProps;
  containerId?: string;
  value?: any;
  width?: string;
  height?: string;
  styles?: any;
  onChange?: (value: any) => void;
  children: React.ReactNode;
  color?: string;
  weight?: string;
  disabled?: boolean;
  defaultValue?: string;
  showSearch?: boolean;
  filterOption?: any;
}

export interface OptionProps {
  value: string;
  children: React.ReactNode;
}
