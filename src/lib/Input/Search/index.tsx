import * as React from 'react';

import { Icon, IconProps } from '../../Icon';
import { InputSize } from '../Input';

import * as S from './styles';

const Search = ({ icon, ...props }: SearchProps): JSX.Element => (
  <S.Search {...props} {...(icon && { prefix: <Icon {...icon}>{icon.children}</Icon> })} />
);

export default Search;

export interface SearchProps {
  placeholder?: string;
  enterButton?: React.ReactNode;
  disabled?: boolean;
  icon?: IconProps;
  value?: string;
  defaultValue?: string;
  loading?: boolean;
  onSearch?: (value: any, e?: any) => void;
  onChange?: (e?: any) => void;
  onPressEnter?: (e?: any) => void;
  size?: InputSize;
  width?: string;
  height?: string;
  background?: string;
  fontSize?: string;
  fontWeight?: string;
  styles?: any;
}
