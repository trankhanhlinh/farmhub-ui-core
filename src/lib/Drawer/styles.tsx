import styled from 'styled-components';
import { Drawer as AntDrawer } from 'antd';

export const Drawer = styled(AntDrawer)<{
  styles?: any;
}>`
  ${p => p.styles}
`;
