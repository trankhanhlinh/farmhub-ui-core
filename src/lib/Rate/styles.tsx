import styled from 'styled-components';

export const Wrapper = styled.div<{
  styles?: any;
}>`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  ${p => p.styles}
`;
