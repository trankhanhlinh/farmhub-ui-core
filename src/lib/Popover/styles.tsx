import styled from 'styled-components';
import { Popover as AntPopover } from 'antd';

export const Popover = styled(AntPopover)<{
  width?: string;
  height?: string;
  padding?: string;
  styles?: any;
}>`
  .ant-popover {
    width: ${p => p.width || 'fit-content'};
    height: ${p => p.height || 'fit-content'};
    .ant-popover-content {
      height: 100%;
      .ant-popover-inner {
        height: 100%;
        .ant-popover-inner-content {
          padding: ${p => p.padding || '12px 16px'};
        }
      }
    }
    ${p => p.styles}
  }
`;
