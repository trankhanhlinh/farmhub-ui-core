import * as React from 'react';

import * as S from './styles';

const Tag = ({ children, ...props }: TagProps): JSX.Element => (
  <S.Tag {...props}>{children}</S.Tag>
);

export default Tag;

export interface TagProps {
  color?: string;
  closable?: boolean;
  children?: React.ReactNode;
  onClose?: (e: any) => void;
  styles?: any;
}
