import styled from 'styled-components';

import { theme } from 'src/theme';

export const Wrapper = styled.div<{
  height: number;
}>`
  width: 100%;
  height: ${p => `${p.height}px`};
  background: #e1e1e1;
  border-radius: 999px;

  position: relative;
`;

export const Percent = styled.div<{
  width: number;
  color?: string;
}>`
  width: ${p => `${p.width}%`};
  height: 100%;
  background: ${p => p.color || theme.colors.error.main};
  border-radius: 999px 0px 0px 999px;

  position: absolute;
  top: 0;
  left: 0;
`;

export const Info = styled.div`
  width: 100%;
  height: 100%;

  padding: 0px 6px;

  position: absolute;
  top: 0;
  left: 0;

  box-sizing: border-box;
`;
