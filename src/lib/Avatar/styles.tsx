import styled from 'styled-components';
import { Avatar as AntAvatar } from 'antd';

export const AvatarGroup = styled(AntAvatar.Group)<{
  styles?: any;
}>`
  ${p => p.styles}
`;

export const Avatar: any = styled(AntAvatar)<{
  styles?: any;
}>`
  ${p => p.styles}
`;
