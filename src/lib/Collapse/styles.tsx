import styled from 'styled-components';
import { Collapse as AntCollapse } from 'antd';

export const Collapse: any = styled(AntCollapse)<{
  styles?: any;
}>`
  ${p => p.styles}
`;

export const Panel = styled(AntCollapse.Panel)<{
  styles?: any;
}>`
  ${p => p.styles}
`;
