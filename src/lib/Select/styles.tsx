import styled from 'styled-components';
import { Select as AntSelect } from 'antd';

export const Select: any = styled(AntSelect)<{
  width?: string;
  height?: string;
  color?: string;
  weight?: string;
  styles?: any;
}>`
  font-weight: ${p => p.theme.font.weight[p.weight || 'regular']};
  font-size: ${p => p.theme.font.size.xs} !important;
  color: ${p => p.theme.colors.text[p.color || 'primary']};

  .ant-select-selector {
    display: flex;
    align-items: center;
    ${p => p.height && `height: 100% !important;`}

    .ant-select-selection-search-input {
      ${p => p.height && `height: 100% !important;`}
      ${p => p.height && `line-height: ${p.height};`}
    }

    .ant-select-selection-placeholder {
      ${p => p.height && `line-height: ${p.height};`}
      font-weight: ${p => p.theme.font.weight.regular} !important;
    }

    .ant-select-selection-item {
      ${p => p.height && `line-height: ${p.height};`}
    }
  }
  ${p => p.height && `height: ${p.height};`}
  ${p => p.width && `width: ${p.width} !important;`}
  ${p => p.styles}
`;
