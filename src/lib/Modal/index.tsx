import * as React from 'react';

import * as S from './styles';

const Modal = ({ children, footer, ...props }: ModalProps): JSX.Element => (
  <S.Modal {...props} footer={footer} {...(footer && { displayFooter: true })}>
    {children}
  </S.Modal>
);

export default Modal;

export interface ModalProps {
  title?: React.ReactNode;
  visible: boolean;
  okText?: string;
  cancelText?: string;
  onOk?: (args?: any) => void;
  onCancel: (args?: any) => void;
  afterClose?: (args?: any) => void;
  maskClosable?: boolean;
  width?: string;
  height?: string;
  bodyPadding?: string;
  displayFooter?: boolean;
  styles?: any;
  children: React.ReactNode;
  centered?: boolean;
  footer?: any;
  bodyHeight?: string;
}
