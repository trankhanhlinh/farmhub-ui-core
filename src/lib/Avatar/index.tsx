import * as React from 'react';

import { Icon, IconProps } from '../Icon';

import * as S from './styles';

export enum AvatarShape {
  Circle = 'circle',
  Square = 'square',
}

export const AvatarGroup = ({ children, ...props }: AvatarGroupProps): JSX.Element => (
  <S.AvatarGroup {...props}>{children}</S.AvatarGroup>
);

const Avatar = ({ icon, ...props }: AvatarProps): JSX.Element => (
  <S.Avatar
    {...props}
    {...(icon && {
      icon: (
        <Icon {...icon} styles={{ ...(icon.styles || {}), display: 'inline-block' }}>
          {icon.children}
        </Icon>
      ),
    })}
  />
);

export default Avatar;

export interface AvatarProps {
  gap?: number;
  icon?: IconProps;
  shape?: AvatarShape;
  size?: any;
  src?: string | React.ReactNode;
  children?: React.ReactNode;
  styles?: any;
}

export interface AvatarGroupProps {
  maxCount?: number;
  maxStyle?: any;
  size?: any;
  children: React.ReactNode;
  styles?: any;
}
