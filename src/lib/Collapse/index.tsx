import * as React from 'react';

import * as S from './styles';

export const CollapsePanel = ({ children, ...props }: CollapsePanelProps): JSX.Element => (
  <S.Panel {...props}>{children}</S.Panel>
);

const Collapse = ({ children, ...props }: CollapseProps): JSX.Element => <S.Collapse {...props}>{children}</S.Collapse>;

export default Collapse;

export interface CollapseProps {
  ghost?: boolean;
  children: React.ReactNode;
  styles?: any;
}

export interface CollapsePanelProps {
  key: string | number;
  header: React.ReactNode;
  children: React.ReactNode;
  styles?: any;
}
