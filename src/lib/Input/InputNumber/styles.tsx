import styled from 'styled-components';
import { InputNumber as AntInputNumber } from 'antd';

import { theme } from 'src/theme';

export const InputNumber = styled(AntInputNumber)<{
  width?: string;
  height?: string;
  background?: string;
  color?: string;
  fontSize?: string;
  fontWeight?: string;
  styles?: any;
}>`
  width: ${p => p.width || 'auto'};
  height: ${p => p.height || '48px'};
  background: ${p => p.background || '#fff'};
  color: ${p => p.theme.colors.text[p.color || 'primary']};
  .ant-input-number-input-wrap {
    height: ${p => p.height || '48px'};

    .ant-input-number-input {
      height: ${p => p.height || '48px'};
      font-size: ${p => theme.font.size[p.fontSize || 'xs']};
      font-weight: ${p => theme.font.weight[p.fontWeight || 'regular']};
    }
  }

  ${p => p.styles}
`;
