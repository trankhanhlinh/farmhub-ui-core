import styled, { css } from 'styled-components';
import { Button as AntButton } from 'antd';

const LinkButtonStyles = css<{
  padding?: string;
}>`
  padding: ${p => p.padding || '0px'} !important;
  border: none;
  border-radius: 0px;
`;

export const Button: any = styled(AntButton)<{
  width?: string;
  height?: string;
  weight?: string;
  type: string;
  styles?: any;
}>`
  ${p => p.width && `width: ${p.width}`};
  height: ${p => p.height || 'auto'};
  font-weight: ${p => p.theme.font.weight[p.weight || 'bold']};
  span {
    font-weight: ${p => p.theme.font.weight[p.weight || 'bold']};
  }
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  ${p => p.type === 'link' && LinkButtonStyles}
  ${p => p.styles}
`;
