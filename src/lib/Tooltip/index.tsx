import * as React from 'react';
import { Tooltip as AntTooltip } from 'antd';

const Tooltip = ({ children, ...props }: TooltipProps): JSX.Element => <AntTooltip {...props}>{children}</AntTooltip>;

export default Tooltip;

export enum TooltipPlacement {
  Top = 'top',
  Left = 'left',
  Right = 'right',
  Bottom = 'bottom',
  TopLeft = 'topLeft',
  TopRight = 'topRight',
  BottomLeft = 'bottomLeft',
  BottomRight = 'bottomRight',
  LeftTop = 'leftTop',
  LeftBottom = 'leftBottom',
  RightTop = 'rightTop',
  RightBottom = 'rightBottom',
}

export interface TooltipProps {
  title: any;
  color?: string;
  visible?: boolean;
  trigger?: 'hover' | 'focus' | 'click' | 'contextMenu' | Array<string>;
  placement?: TooltipPlacement;
  children: React.ReactNode;
}
