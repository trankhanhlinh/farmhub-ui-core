import styled, { css } from 'styled-components';

const Text = styled.span<TextProps>`
  word-break: break-word;
  color: ${p => p.theme.colors.text[p.color || 'primary']};
  line-height: ${p => p.lineHeight || '26px'};
  font-size: ${p => p.theme.font.size[p.size || 'xs']};
  font-weight: ${p => p.theme.font.weight[p.weight || 'regular']} !important;
  ${p =>
    p.textTransform &&
    css`
      text-transform: ${p.textTransform};
    `};

  white-space: pre-wrap;
  overflow: hidden;
  text-overflow: ellipsis;
  -webkit-line-clamp: ${p => p.lineNumber || 1};
  -webkit-box-orient: vertical;
  display: -webkit-box;

  ${p =>
    p.capitalizeFirstLetter &&
    css`
      display: inline-block;
      &::first-letter {
        text-transform: capitalize !important;
      }
    `};

  ${p => p.styles}
`;

export default Text;

export interface TextProps {
  color?: string;
  lineHeight?: string;
  lineNumber?: number;
  size?: string;
  weight?: string;
  textTransform?: string;
  capitalizeFirstLetter?: boolean;
  styles?: any;
}
