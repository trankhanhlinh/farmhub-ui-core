import styled from 'styled-components';
import { Pagination as AntPagination } from 'antd';

export const Pagination = styled(AntPagination)<{
  styles?: any;
}>`
  ${p => p.styles}
`;
