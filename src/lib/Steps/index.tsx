import * as React from 'react';

import { Icon, IconProps } from '../Icon';

import * as S from './styles';

export const Steps = ({ children, ...props }: StepsProps): JSX.Element => <S.Steps {...props}>{children}</S.Steps>;

export default Steps;

export const Step = ({ icon, ...props }: StepProps): JSX.Element => (
  <S.Step {...props} {...(icon && { icon: <Icon {...icon}>{icon.children}</Icon> })} />
);

export enum StepsDirection {
  Vertical = 'vertical',
  Horizontal = 'horizontal',
}

export enum StepsSize {
  Default = 'default',
  Small = 'small',
}

export enum StepsStatus {
  Wait = 'wait',
  Process = 'process',
  Finish = 'finish',
  Error = 'error',
}

export interface StepProps {
  title: React.ReactNode;
  subTitle?: React.ReactNode;
  description?: React.ReactNode;
  disabled?: boolean;
  icon?: IconProps;
  status?: StepsStatus;
  styles?: any;
}

export interface StepsProps {
  current?: number;
  direction?: StepsDirection;
  labelPlacement?: StepsDirection;
  percent?: number;
  size?: StepsSize;
  status?: StepsStatus;
  onChange?: (current: number) => void;
  children: React.ReactNode;
  styles?: any;
}
