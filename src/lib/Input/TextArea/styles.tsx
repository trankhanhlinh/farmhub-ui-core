import styled from 'styled-components';
import { Input as AntInput } from 'antd';

import { theme } from 'src/theme';

export const TextArea = styled(AntInput.TextArea)<{
  width?: string;
  height?: string;
  background?: string;
  fontSize?: string;
  fontWeight?: string;
  styles?: any;
}>`
  width: ${p => p.width || '100%'};
  height: ${p => p.height || '48px'};
  background: ${p => p.background || '#fff'};

  .ant-input {
    background: ${p => p.background || '#fff'};
    font-size: ${p => theme.font.size[p.fontSize || 'xs']};
    font-weight: ${p => theme.font.weight[p.fontWeight || 'regular']};
    &::placeholder {
      font-size: ${p => theme.font.size[p.fontSize || 'xs']};
    }
  }
  ${p => p.styles}
`;
