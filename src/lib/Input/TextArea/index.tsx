import * as React from 'react';

import { InputSize } from '../Input';

import * as S from './styles';

const TextArea = ({ rows = 1, ...props }: TextAreaProps): JSX.Element => <S.TextArea rows={rows} {...props} />;

export default TextArea;

export interface TextAreaProps {
  rows?: number;
  cols?: number;
  placeholder?: string;
  disabled?: boolean;
  maxLength?: number;
  value?: string;
  defaultValue?: string;
  onChange?: (e?: any) => void;
  onPressEnter?: (e?: any) => void;
  onFocus?: (e?: any) => void;
  size?: InputSize;
  width?: string;
  height?: string;
  background?: string;
  fontSize?: string;
  fontWeight?: string;
  styles?: any;
}
