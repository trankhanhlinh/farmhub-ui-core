import * as React from 'react';

import * as S from './styles';

export enum InputSize {
  Large = 'large',
  Middle = 'middle',
  Small = 'small',
}

const Input = ({ ...props }: InputProps): JSX.Element => <S.Input {...props} />;

export default Input;

export interface InputProps {
  placeholder?: string;
  disabled?: boolean;
  suffix?: React.ReactNode;
  prefix?: React.ReactNode;
  maxLength?: number;
  value?: string;
  defaultValue?: string;
  onChange?: (e?: any) => void;
  onPressEnter?: (e?: any) => void;
  onClick?: (e?: any) => void;
  onFocus?: (e?: any) => void;
  onBlur?: (e?: any) => void;
  size?: InputSize;
  width?: string;
  height?: string;
  background?: string;
  fontSize?: string;
  fontWeight?: string;
  styles?: any;
}
