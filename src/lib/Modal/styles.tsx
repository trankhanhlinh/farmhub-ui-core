import styled from 'styled-components';
import { Modal as AntModal } from 'antd';

import { theme } from 'src/theme';

export const Modal = styled(AntModal)<{
  width?: string;
  height?: string;
  bodyPadding?: string;
  bodyHeight?: string;
  displayFooter?: boolean;
  styles?: any;
}>`
  ${p => p.width && `width: ${p.width} !important;`}
  height: ${p => p.height || 'unset'};

  .ant-modal-content {
    ${p => p.width && `width: ${p.width};`}
    height: ${p => p.height || 'unset'};

    .ant-modal-title {
      text-align: center;
      font-size: ${theme.font.size.md};
      font-weight: ${theme.font.weight.bold};
    }

    .ant-modal-body {
      height: ${p => p.bodyHeight || '580px'};
      overflow-y: auto;
      overflow-x: hidden;
      padding: ${p => p.bodyPadding || '24px'};
      &::-webkit-scrollbar-track {
        background-color: #ffffff;
      }

      &::-webkit-scrollbar {
        width: 6px;
        background-color: #ffffff;
      }

      &::-webkit-scrollbar-thumb {
        background-color: #e0e0e0;
      }
    }

    .ant-modal-footer {
      display: ${p => (p.displayFooter ? 'flex' : 'none')};
      justify-content: flex-end;
      .ant-btn {
        width: 108px;
        height: 42px;

        span {
          font-size: ${p => p.theme.font.size.xs};
        }

        &:first-child {
          border: 0;
        }
      }
    }
  }
  ${p => p.styles}
`;
