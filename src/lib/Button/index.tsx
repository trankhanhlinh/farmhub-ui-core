import * as React from 'react';

import { Icon, IconProps } from '../Icon';
import Row from '../Row';
import Space from '../Space';
import Tooltip from '../Tooltip'

import * as S from './styles';

export enum ButtonType {
  Default = 'default',
  Primary = 'primary',
  // eslint-disable-next-line @typescript-eslint/no-shadow
  Link = 'link',
  Text = 'text',
  Ghost = 'ghost',
  Dashed = 'dashed',
}

export enum ButtonShape {
  Circle = 'circle',
  Round = 'round',
}

export enum HtmlType {
  Button = 'button',
  Reset = 'reset',
  Submit = 'submit',
}

const Button = ({ children, onClick, ...props }: ButtonProps): JSX.Element => {
  const { type = ButtonType.Primary, href, icon, tooltip, disabled = false, iconSpace = 6 } = props;
  let button = (
    <S.Button
      {...props}
      {...(type !== ButtonType.Link && onClick && { onClick })}
      {...(type === ButtonType.Link && { href: href || (onClick && onClick())})}
      {...(icon && { icon: <Icon {...icon}>{icon.children}</Icon> })}
      type={type}
      disabled={disabled}
    >
      {children && icon ? (
        <Row width="fit-content">
          <Space width={iconSpace} />
          {children}
        </Row>
      ) : (
        children
      )}
    </S.Button>
  );

  if (tooltip) {
    button = <Tooltip title={tooltip}>{button}</Tooltip>;
  }

  return <>{button}</>;
};

export default Button;

export interface ButtonProps {
  type?: ButtonType;
  htmlType?: HtmlType;
  shape?: ButtonShape;
  ghost?: boolean;
  danger?: boolean;
  tooltip?: string;
  icon?: IconProps;
  iconSpace?: number;
  block?: boolean;
  disabled?: boolean;
  height?: string;
  width?: string;
  weight?: string;
  padding?: string;
  loading?: boolean;
  styles?: any;
  onClick?: () => any;
  href?: string;
  className?: string;
  children?: React.ReactNode;
}
