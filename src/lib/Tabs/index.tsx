import * as React from 'react';
import { Tabs as AntTabs } from 'antd';

import * as S from './styles';

export enum TabPosition {
  Top = 'top',
  Right = 'right',
  Bottom = 'bottom',
  Left = 'left',
}

const Tabs = ({
  children,
  defaultActiveKey,
  tabPosition = TabPosition.Top,
  styles,
  ...props
}: TabsProps): JSX.Element => (
  <S.Tabs {...props} defaultActiveKey={defaultActiveKey} tabPosition={tabPosition} styles={styles}>
    {children}
  </S.Tabs>
);

export default Tabs;

export const TabPane = ({ children, tab, key, ...props }: TabPaneProps): JSX.Element => (
  <AntTabs.TabPane {...props} tab={tab} key={key}>
    {children}
  </AntTabs.TabPane>
);

export interface TabPaneProps {
  tab: string;
  key: string;
  children: React.ReactNode;
}

export interface TabsProps {
  defaultActiveKey: string;
  tabPosition?: TabPosition;
  width?: string;
  fontSize?: string;
  fontWeight?: string;
  tabPadding?: string;
  tabPaneActivePadding?: string;
  styles?: any;
  children: React.ReactNode;
  activeKey?: string;
  marginBottom?: string;
  tabBarExtraContent?: React.ReactNode;
  onChange?: (e: string) => void;
}
