import React from 'react';
import * as S from './styles';

const Spacing = ({
  size,
  direction,
  children,
  styles,
  ...props
}: SpacingProps): JSX.Element => (
  <S.Spacing direction={direction} styles={styles} size={size} {...props}>
    {children}
  </S.Spacing>
);

export default Spacing;

export interface SpacingProps {
  size?: 'small' | 'middle' | 'large' | number;
  direction?: 'vertical' | 'horizontal';
  styles?: any;
  children?: any;
}
