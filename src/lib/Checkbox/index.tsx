import * as React from 'react';
import { Checkbox as AntCheckbox } from 'antd';

const Checkbox = ({ children, disabled = false, onChange }: CheckboxProps): JSX.Element => (
  <AntCheckbox disabled={disabled} {...(onChange && { onChange })}>
    {children}
  </AntCheckbox>
);

export default Checkbox;

export interface CheckboxProps {
  disabled?: boolean;
  children: React.ReactNode;
  onChange?: (e: any) => void;
}
