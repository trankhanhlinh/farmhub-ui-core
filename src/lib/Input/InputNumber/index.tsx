import * as React from 'react';

import { InputSize } from '../Input';

import * as S from './styles';

const InputNumber = ({ ...props }: InputNumberProps): JSX.Element => <S.InputNumber {...props} />;

export default InputNumber;

export interface InputNumberProps {
  placeholder?: string;
  controls?: boolean;
  decimalSeparator?: string;
  disabled?: boolean;
  value?: string;
  defaultValue?: number;
  formatter?: (value: any) => string;
  keyboard?: boolean;
  min?: number;
  max?: number;
  step?: number;
  onChange?: (value: number | string | null) => void;
  onPressEnter?: (e?: any) => void;
  size?: InputSize;
  width?: string;
  height?: string;
  background?: string;
  color?: string;
  fontSize?: string;
  fontWeight?: string;
  ref?: any;
  onBlur?: (e: any) => void;
  styles?: any;
}
