import * as React from 'react';
import { Rate as AntRate } from 'antd';

import Space from '../Space';
import Text, { TextProps } from '../Text';

import * as S from './styles';

const Rate = ({
  allowHalf = false,
  disabled = false,
  defaultValue = 0,
  votes,
  space = 6,
  textProps = {},
  styles,
}: RateProps): JSX.Element => (
  <S.Wrapper styles={styles}>
    <AntRate allowHalf={allowHalf} disabled={disabled} defaultValue={defaultValue} />
    {votes != null && (
      <>
        <Space width={space} />
        <Text {...textProps}>({votes})</Text>
      </>
    )}
  </S.Wrapper>
);

export default Rate;

export interface RateProps {
  allowHalf?: boolean;
  disabled?: boolean;
  defaultValue?: number;
  votes?: number;
  space?: number;
  textProps?: TextProps;
  styles?: any;
}
