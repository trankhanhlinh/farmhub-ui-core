import styled from 'styled-components';

import { theme } from 'src/theme';

const Space = styled.div<{
  width?: number;
  height?: number;
}>`
  width: ${p => (p.width != null ? `${p.width}px` : theme.spacing.xxs)};
  height: ${p => (p.height != null ? `${p.height}px` : theme.spacing.xxs)};
  flex-shrink: 0;
`;

export default Space;
