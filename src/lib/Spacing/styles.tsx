import { Space as AntSpace } from 'antd';
import styled from 'styled-components';

export const Spacing = styled(AntSpace)<{ styles?: any }>`
  ${p => p.styles}
`;
