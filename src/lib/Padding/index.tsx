import styled from 'styled-components';

const Padding = styled.div<{
  width?: string;
  padding?: string;
}>`
  width: ${props => props.width || '100%'};
  padding: ${props => props.padding || '0'};
`;

export default Padding;
