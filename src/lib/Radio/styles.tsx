import styled from 'styled-components';
import { Radio as AntRadio } from 'antd';

export const RadioGroup = styled(AntRadio.Group)<{
  styles?: any;
}>`
  ${p => p.styles}
`;

export const Radio: any = styled(AntRadio)<{
  styles?: any;
}>`
  ${p => p.styles}
`;
