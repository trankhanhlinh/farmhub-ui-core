import * as React from 'react';
import { Dropdown as AntDropdown } from 'antd';

import { Icon, IconProps } from '../Icon';
import Row from '../Row';
import Space from '../Space';

import * as S from './styles';

export const DropdownItem = ({
  disabled = false,
  icon,
  key,
  children,
  onClick,
  iconSpace = 6,
  ...props
}: DropdownItemProps): JSX.Element => (
  <S.MenuItem
    {...props}
    disabled={disabled}
    {...(icon && { icon: <Icon {...icon}>{icon.children}</Icon> })}
    key={key}
    onClick={onClick}
  >
    {children && icon ? (
      <Row width="fit-content">
        <Space width={iconSpace} />
        {children}
      </Row>
    ) : (
      children
    )}
  </S.MenuItem>
);

export enum TriggerMode {
  Click = 'click',
  Hover = 'hover',
}

export enum DropdownPlacement {
  BottomLeft = 'bottomLeft',
  BottomCenter = 'bottomCenter',
  BottomRight = 'bottomRight',
  TopLeft = 'topLeft',
  TopCenter = 'topCenter',
  TopRight = 'topRight',
}

const Dropdown = ({
  placement = DropdownPlacement.BottomLeft,
  children,
  items,
  modes = [TriggerMode.Click],
  ...props
}: DropdownProps): JSX.Element => {
  const { containerId, visible, onVisibleChange, menu } = props;
  const overlay = <S.Menu {...menu}>{items}</S.Menu>;
  return (
    <AntDropdown
      placement={placement}
      overlay={overlay}
      trigger={modes}
      {...(containerId && { getPopupContainer: () => document.getElementById(containerId) as HTMLElement })}
      {...(visible && { visible })}
      {...(onVisibleChange && { onVisibleChange })}
    >
      {children}
    </AntDropdown>
  );
};

export default Dropdown;

export interface DropdownProps {
  placement?: DropdownPlacement;
  children: React.ReactNode;
  items: React.ReactNode;
  modes?: TriggerMode[];
  containerId: string;
  visible?: boolean;
  onVisibleChange?: (visible: boolean) => void;
  menu?: {
    width?: string;
    padding?: string;
    styles?: any;
  };
}

export interface DropdownItemProps {
  disabled?: boolean;
  icon?: IconProps;
  iconSpace?: number;
  key: string;
  onClick: (...args: any[]) => any;
  padding?: string;
  styles?: any;
  children: React.ReactNode;
}
