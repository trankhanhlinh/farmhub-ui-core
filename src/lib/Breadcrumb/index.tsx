import * as React from 'react';
import { Breadcrumb as AntBreadcrumb } from 'antd';

export const BreadcrumbItem = ({ children, href, onClick, ...props }: BreadcrumbItemProps): JSX.Element => (
  <AntBreadcrumb.Item {...(href && { href })} {...(onClick && { onClick })} {...props}>
    {children}
  </AntBreadcrumb.Item>
);

const Breadcrumb = ({ children, separator = '/', ...props }: BreadcrumbProps): JSX.Element => (
  <AntBreadcrumb separator={separator} {...props}>
    {children}
  </AntBreadcrumb>
);

export default Breadcrumb;

export interface BreadcrumbProps {
  separator?: React.ReactNode;
  children: React.ReactNode;
}

export interface BreadcrumbItemProps {
  href?: string;
  onClick?: VoidFunction;
  children: React.ReactNode;
}
