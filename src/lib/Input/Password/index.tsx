import * as React from 'react';

import { Icon, IconProps } from '../../Icon';
import { InputSize } from '../Input';

import * as S from './styles';

const Password = ({ icon, ...props }: PasswordProps): JSX.Element => (
  <S.Password {...props} {...(icon && { prefix: <Icon {...icon}>{icon.children}</Icon> })} />
);

export default Password;

export interface PasswordProps {
  placeholder?: string;
  disabled?: boolean;
  icon?: IconProps;
  iconRender?: (visible: boolean) => React.ReactNode;
  visibilityToggle?: boolean;
  onChange?: (e?: any) => void;
  size?: InputSize;
  width?: string;
  height?: string;
  background?: string;
  fontSize?: string;
  fontWeight?: string;
  styles?: any;
}
