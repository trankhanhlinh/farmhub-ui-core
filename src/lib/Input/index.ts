export { default as Input } from './Input';
export { default as InputNumber } from './InputNumber';
export { default as Password } from './Password';
export { default as Search } from './Search';
export { default as TextArea } from './TextArea';

export * from './Input';
export * from './InputNumber';
export * from './Password';
export * from './Search';
export * from './TextArea';
