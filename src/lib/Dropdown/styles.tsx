import styled from 'styled-components';
import { Menu as AntMenu } from 'antd';

import { theme } from 'src/theme';

export const Menu = styled(AntMenu)<{
  width?: string;
  padding?: string;
  styles?: any;
}>`
  ${p => p.width && `width: ${p.width};`}
  ${p => p.padding && `padding: ${p.padding};`}
  ${p => p.styles}
`;

export const MenuItem = styled(AntMenu.Item)<{
  padding?: string;
  styles?: any;
}>`
  padding: ${p => p.padding || '18px 24px'};
  font-weight: ${theme.font.weight.medium};
  display: flex;
  align-items: center;
  ${p => p.styles}
`;
