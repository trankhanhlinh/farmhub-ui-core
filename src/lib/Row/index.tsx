import styled from 'styled-components';

const Row = styled.div<{
  width?: string;
  height?: string;
  justifyContent?: string;
  alignItems?: string;
  wrap?: string;
  styles?: any;
}>`
  width: ${p => p.width || '100%'};
  height: ${p => p.height || 'unset'};

  display: flex;
  justify-content: ${p => p.justifyContent || 'flex-start'};
  align-items: ${p => p.alignItems || 'center'};

  flex-wrap: ${p => p.wrap || 'nowrap'};

  ${p => p.styles}
`;

export default Row;
