import * as React from 'react';
import { Space } from 'antd';

import * as S from './styles';

export const RadioGroup = ({ vertical, gap = 8, children, ...props }: RadioGroupProps): JSX.Element => (
  <S.RadioGroup {...props}>
    {vertical ? (
      <Space size={gap} direction="vertical">
        {children}
      </Space>
    ) : (
      children
    )}
  </S.RadioGroup>
);

const Radio = ({ children, ...props }: RadioProps): JSX.Element => <S.Radio {...props}>{children}</S.Radio>;

export default Radio;

export interface RadioProps {
  checked?: boolean;
  defaultChecked?: boolean;
  disabled?: boolean;
  value?: any;
  children: React.ReactNode;
  styles?: any;
}

export interface RadioGroupProps {
  vertical?: boolean;
  gap?: number;
  defaultValue?: any;
  value?: any;
  disabled?: boolean;
  onChange?: (e?: any) => void;
  children: React.ReactNode;
  styles?: any;
}
