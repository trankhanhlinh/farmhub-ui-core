import * as React from 'react';

import * as S from './styles';

const Popover = ({
  placement = PopoverPlacement.Top,
  trigger = 'hover',
  children,
  containerId,
  ...props
}: PopoverProps): JSX.Element => (
  <S.Popover
    {...props}
    placement={placement}
    trigger={trigger}
    getPopupContainer={triggerNode =>
      containerId ? (document.getElementById(containerId) as HTMLElement) : triggerNode
    }
  >
    {children}
  </S.Popover>
);

export default Popover;

export enum PopoverPlacement {
  Top = 'top',
  Left = 'left',
  Right = 'right',
  Bottom = 'bottom',
  TopLeft = 'topLeft',
  TopRight = 'topRight',
  BottomLeft = 'bottomLeft',
  BottomRight = 'bottomRight',
  LeftTop = 'leftTop',
  LeftBottom = 'leftBottom',
  RightTop = 'rightTop',
  RightBottom = 'rightBottom',
}

export interface PopoverProps {
  placement?: PopoverPlacement;
  title?: string;
  content: React.ReactNode;
  trigger?: string;
  visible?: boolean;
  onVisibleChange?: (visible: boolean) => void;
  children: React.ReactNode;
  containerId?: string;
  width?: string;
  height?: string;
  padding?: string;
  styles?: any;
}
