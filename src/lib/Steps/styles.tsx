import styled from 'styled-components';
import { Steps as AntSteps } from 'antd';

export const Steps: any = styled(AntSteps)<{
  styles?: any;
}>`
  ${p => p.styles}
`;

export const Step = styled(AntSteps.Step)<{
  styles?: any;
}>`
  ${p => p.styles}
`;
