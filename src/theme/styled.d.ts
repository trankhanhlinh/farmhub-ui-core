// #region Global Imports
import 'styled-components';
// #endregion Global Imports

declare module 'styled-components' {
  export interface DefaultTheme {
    font: {
      family: string;
      weight: {
        [key: string]: string;
        regular: string;
        light: string;
        medium: string;
        bold: string;
      };
      size: {
        [key: string]: string;
        tiny: string;
        xxs: string;
        xs: string;
        sm: string;
        md: string;
        lg: string;
        xl: string;
      };
    };

    /**
     * Colors
     */
    colors: {
      black: string;
      white: string;
      success: string;
      body: string;
      price: string;

      primary: {
        [key: string]: string;
        light: string;
        main: string;
        dark: string;
        contrastText: string;
      };

      secondary: {
        [key: string]: string;
        light: string;
        main: string;
        dark: string;
        contrastText: string;
      };

      text: {
        [key: string]: string;
        primary: string;
        secondary: string;
        disabled: string;
        hint: string;
      };

      border: {
        light: string;
        main: string;
        dark: string;
        green: string;
      };

      skeleton: {
        background: string;
        foreground: string;
      };

      navbar: {
        main: string;
      };

      dropdown: {
        main: string;
      };

      error: {
        [key: string]: string;
        light: string;
        main: string;
        dark: string;
        contrastText: string;
      };

      warning: string;

      info: string;

      grey: {
        50: string;
        100: string;
        200: string;
        300: string;
        400: string;
        500: string;
        600: string;
        700: string;
        800: string;
        900: string;
      };
    };

    /**
     * Shadows
     */
    shadows: {
      sm: string;
      md: string;
      lg: string;
      xl: string;
    };

    /**
     * Breakpoints
     */
    screen: {
      xs: string;
      sm: string;
      md: string;
      lg: string;
      xl: string;
    };

    /**
     * Spacing
     */
    spacing: {
      [key: string]: string;
      none: string;
      xxs: string;
      xs: string;
      sm: string;
      md: string;
      lg: string;
      xl: string;
    };

    /**
     * Border radius
     */
    radius: {
      sm: string;
      md: string;
      lg: string;
    };

    /**
     * z-index
     */
    zIndex: {
      xs: number;
      sm: number;
      md: number;
      lg: number;
      xl: number;
    };

    /**
     * Transition
     */
    transition: {
      duration: string;
    };
  }
}
